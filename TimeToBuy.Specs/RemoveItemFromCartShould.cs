﻿using Microsoft.EntityFrameworkCore;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimeToBuy.Web.Domain;
using TimeToBuy.Web.Features.Cart;
using Xunit;

namespace TimeToBuy.Specs
{
    public class RemoveItemFromCartShould
    {
        private DbContextOptions<StoreContext> options;

        public RemoveItemFromCartShould()
        {
            options = new DbContextOptionsBuilder<StoreContext>()
                .UseInMemoryDatabase(databaseName: "RemoveFromCart")
                .Options;
        }

        [Fact]
        public void RemoveItemFromCart()
        {
            var sessionId = Guid.NewGuid();

            using (var context = new StoreContext(options))
            {
                var cart = new ShoppingCart
                {
                    SessionId = sessionId,
                    Items = new List<CartLineItem> {
                        new CartLineItem { ProductId = 1, Quantity = 1 },
                        new CartLineItem { ProductId = 2, Quantity = 2 }
                    }
                };

                context.ShoppingCart.Add(cart);
                context.SaveChanges();
            }

            using (var context = new StoreContext(options))
            {
                var cartService = new CartService(context);
                cartService.RemoveItem(new RemoveItemCommand { ProductId = 1, SessionId = sessionId });
            }

            using (var context = new StoreContext(options))
            {
                var cart = context
                    .ShoppingCart
                    .Include(x => x.Items)
                    .SingleOrDefault(x => x.SessionId == sessionId);

                cart.Items.Count(x => x.ProductId == 1).ShouldBe(0);
                cart.Items.Count(x => x.ProductId == 2).ShouldBe(1);
            }
        }
    }
}
