﻿import React from 'react';
import { CardElement, injectStripe } from "react-stripe-elements";
import { Button } from "reactstrap";
import './CheckoutForm.css';

class CheckoutForm extends React.Component {

    submit = async (ev) => {
        // remember to plug in real customer name here!
        let { token } = await this.props.stripe.createToken({ name: 'Name' });
        if (this.props.onPaymentMethodChanged)
            this.props.onPaymentMethodChanged(token);
    }

    render() {
        return (
            <div className='checkout'>                
                <CardElement style={{ base: { fontSize: '18px' } }} />
                <Button color='primary' onClick={this.submit}>Place Order</Button>
            </div>
        )
    }
}

export default injectStripe(CheckoutForm);