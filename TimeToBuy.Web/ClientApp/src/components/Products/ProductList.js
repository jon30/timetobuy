﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Axios from 'axios';

export default class ProductList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            products: []
        };
    }

    async componentDidMount() {
        // make API call
        let result = await Axios('/api/product');
        this.setState({ 'products': result.data.products });

    }

    render() {
        return (
            <div className="row">
                {this.state.products.map(product =>
                    <div key={product.id} className="col-4">
                        <div className="card product-card">
                            <div className="card-body">
                                <h5 className="card-title">{product.name}</h5>
                                <p className="card-text">{product.description}</p>
                                <Link to={`/product/${product.id}`}>View</Link>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        );
    }

}