﻿using System;

namespace TimeToBuy.Web.Features.Cart
{
    public class AddItemCommand
    {
        public int ProductId { get; set; }
        public Guid? SessionId { get; set; }
    }
}