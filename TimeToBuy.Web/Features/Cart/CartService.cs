﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TimeToBuy.Web.Domain;

namespace TimeToBuy.Web.Features.Cart
{
    public class CartService
    {
        private readonly StoreContext dbContext;

        public CartService(StoreContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public AddItemResult AddItem(AddItemCommand command)
        {
            // if sessionId is provided, look it up
            // add item to the existing cart
            // return existing cart sessionId

            ShoppingCart cart;

            if (command.SessionId.HasValue)
            {
                cart = dbContext
                    .ShoppingCart
                    .Include(x=>x.Items)
                    .SingleOrDefault(x => x.SessionId == command.SessionId);
            } else
            {
                Guid newCartSessionId = Guid.NewGuid();
                cart = new ShoppingCart { SessionId = newCartSessionId, CreatedOn = DateTime.Now };
                dbContext.ShoppingCart.Add(cart);
            }

            cart.Add(command.ProductId);
            dbContext.SaveChanges();

            return new AddItemResult { SessionId = cart.SessionId };
        }

        public void RemoveItem(RemoveItemCommand removeItemCommand)
        {
            var cart = dbContext
                .ShoppingCart
                .Include(x=>x.Items)
                .SingleOrDefault(x => x.SessionId == removeItemCommand.SessionId);

            if (cart == null)
            {
                return;
            }

            cart.Remove(removeItemCommand.ProductId);
            dbContext.SaveChanges();
        }      

        public GetCartResponse Get(Guid? sessionId)
        {
            if (!sessionId.HasValue)
            {
                return new GetCartResponse();
            }

            var model = new GetCartResponse();
            var cart = dbContext
                .ShoppingCart
                .Include(x=>x.Items)
                .SingleOrDefault(x => x.SessionId == sessionId);

            foreach (var item in cart.Items)
            {
                var itemDetails = dbContext.Products.Find(item.ProductId);

                model.Items.Add(new GetCartResponse.ItemDetails {
                    ProductId = item.ProductId,
                    Quantity = item.Quantity,
                    Name = itemDetails.Name,
                    Price = itemDetails.Price
                });
            }

            return model;
        }

        public class AddItemResult
        {
            public Guid SessionId { get; set; }
        }
    }
}