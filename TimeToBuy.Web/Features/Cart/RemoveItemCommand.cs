﻿using System;

namespace TimeToBuy.Web.Features.Cart
{
    public class RemoveItemCommand
    {
        public Guid SessionId { get; set; }
        public int ProductId { get; set; }
    }
}