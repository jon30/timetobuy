﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TimeToBuy.Web.Migrations
{
    public partial class FixedtypeinCreatedOnfield : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CreateOn",
                table: "ShoppingCart",
                newName: "CreatedOn");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CreatedOn",
                table: "ShoppingCart",
                newName: "CreateOn");
        }
    }
}
